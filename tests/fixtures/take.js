// example of using JSON build-in functions to take property out by key

const resources = [
  {
    name: "A",
    description: "A description",
    link: "https://a.com"
  },
  {
    name: "B",
    description: "B description",
    link: "https://b.com"
  },
  {
    name: "C",
    description: "C description",
    link: "https://c.com"
  }
]

const names = JSON.parse(JSON.stringify(resources, ['name']))

console.log('names', names)

const namesAndLinks = JSON.parse(JSON.stringify(resources, ['name', 'link']))

console.log('names & links', namesAndLinks)
