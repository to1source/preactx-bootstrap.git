# preactx-bootstrap

> Preact.js is like React but so much better. And Bootstrap still one of the most popular UI framework on the net.

## Road Map

- 0.1.0 - just put together all the components (See [development](#development))
- 1.X - with build tool to build your customized version of the UI
- 2.X - with extra integrate Javascript UI elements

## Development

- We use [plop.js](http://plop-js.com) to generate template, it automate most of the template / storybook creation  
- We use [storybook](https://storybook.js.org/) to test and develop the components
- Using ES

---

MIT

to1source / NEWBRAN (c) 2011
