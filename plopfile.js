// making creating template so much simplier
// super cheap and dirty way to grab the input
const { argv } = process;

console.log(argv);

module.exports = function (plop) {
	// 创建生成器
	plop.setGenerator('basics', {
		description: '这是一个基础plopfile模板',
		prompts: [{
      type: 'input',
			name: 'name',
			message: 'Component name please'
    }],
		actions: [{
      type: 'add',
			path: 'src/components/{{name}}/{{name}}.js',
			templateFile: 'plopfiles/templates/Component.hbs'
    }]  
	});
};
